
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Baralho {
	private String[] naipes = {"Paus", "Espadas", "Ouros", "Copas"};
	public ArrayList<Carta> maco = new ArrayList<>();
	public ArrayList<Carta> macoMorto = new ArrayList<>();
	
	public void preencherBaralho() {
		for(String naipe: naipes) {
			for (int i = 1; i < 14; i++) {
				maco.add(new Carta(i, naipe));
			}
		}
	}

	public void embaralhar() {
		Collections.shuffle(maco);
	}

	public Carta retirarCarta() {
		Carta carta = maco.remove(0);
		//	    System.out.println(this);
		return carta;
	}

	public String toString() {
		String texto = "";

		for(Carta carta: maco) {
			texto += carta.toString() + "\n";
		}

		return texto;
	}

}